package com.thecoffee.aoptransaction;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;

@Service
public class TransactionTestService {

	public void saveFromInside() {
		System.out.println("call from inside");
		save();
		privateTransaction();
	}

	public void noTr() {
		System.out.println("no transactional annotation transaction:" + TransactionSynchronizationManager.isActualTransactionActive());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void save() {
		System.out.println("transaction: " + TransactionSynchronizationManager.isActualTransactionActive());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	void privateTr() {
		System.out.println("package private transaction: " + TransactionSynchronizationManager.isActualTransactionActive());
	}

	public void saveFromInsidejavax() {
		System.out.println("javax call from inside");
		savejavax();
		privateTransactionJavax();
	}

	@javax.transaction.Transactional(value = javax.transaction.Transactional.TxType.REQUIRES_NEW)
	public void savejavax() {
		System.out.println("javax transaction: " + TransactionSynchronizationManager.isActualTransactionActive());
	}

	@javax.transaction.Transactional(value = javax.transaction.Transactional.TxType.REQUIRES_NEW)
	void privateTrjavax() {
		System.out.println("javax package private transaction: " + TransactionSynchronizationManager.isActualTransactionActive());
	}

	@javax.transaction.Transactional(value = javax.transaction.Transactional.TxType.REQUIRES_NEW)
	private void privateTransactionJavax() {
		System.out.println("package private transaction: " + TransactionSynchronizationManager.isActualTransactionActive());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	private void privateTransaction() {
		System.out.println("private transaction: " + TransactionSynchronizationManager.isActualTransactionActive());
	}
}


