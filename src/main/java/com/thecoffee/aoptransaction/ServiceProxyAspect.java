package com.thecoffee.aoptransaction;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ServiceProxyAspect {

	@Pointcut("execution(* *(..))")
	private void anyMethod() {}

	@Pointcut("within(com.thecoffee.aoptransaction.TransactionTestService*)")
	private void inService() {}

	@Before("anyMethod() && inService()")
	public void processServiceMethod(JoinPoint joinPoint){
		System.out.println("ANNOTATION TYPE ASPECT PROXY!!!");
	}
}

