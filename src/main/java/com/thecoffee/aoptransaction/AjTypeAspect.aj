package com.thecoffee.aoptransaction;

public aspect AjTypeAspect {

	pointcut anyMethod() : execution(* *(..));

	pointcut withinTestService() : within(com.thecoffee.aoptransaction.TransactionTestService*);

	before() : anyMethod() && withinTestService() {
		System.out.println("ASPECTJ TYPE ASPECT PROXY!");
	}
}
