package com.thecoffee.aoptransaction;


import org.springframework.data.jpa.repository.JpaRepository;

public interface EntityRepository extends JpaRepository<TestEntity, Integer> {
}
