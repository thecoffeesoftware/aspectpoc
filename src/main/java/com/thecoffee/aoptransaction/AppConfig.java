package com.thecoffee.aoptransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.aspectj.AnnotationTransactionAspect;
import org.springframework.transaction.aspectj.JtaAnnotationTransactionAspect;

@Configuration
//we need this config to enable annotation type aspects
@EnableAspectJAutoProxy
//we need this config to enable aspectj driven transaction management (and disable proxy driven)
@EnableTransactionManagement(mode = AdviceMode.ASPECTJ)
public class AppConfig {

	@Autowired
	public AppConfig(PlatformTransactionManager transactionManager) {
		//We must set the transactionManager for this aspect to control its transactions.
		//This aspect is triggered by org.springframework.transaction.annotation.Transactional
		AnnotationTransactionAspect.aspectOf().setTransactionManager(transactionManager);
		//We must set the transactionManager for this aspect to control its transactions.
		//This aspect is triggered by javax.transaction.Transactional
		JtaAnnotationTransactionAspect.aspectOf().setTransactionManager(transactionManager);
	}

}
