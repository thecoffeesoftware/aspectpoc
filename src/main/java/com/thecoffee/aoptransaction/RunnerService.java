package com.thecoffee.aoptransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class RunnerService {

	private TransactionTestService transactionTestService;

	@Autowired
	public RunnerService(TransactionTestService transactionTestService){
		this.transactionTestService = transactionTestService;
	}

	@PostConstruct
	private void fireTransactions(){
		transactionTestService.noTr();
		System.out.println("from outside");
		transactionTestService.save();
		transactionTestService.saveFromInside();
		System.out.println("from outside");
		transactionTestService.privateTr();
		System.out.println("from outside");
		transactionTestService.savejavax();
		transactionTestService.saveFromInsidejavax();
		System.out.println("from outside");
		transactionTestService.privateTrjavax();
	}
}
